import { Component, NgZone } from '@angular/core';
import { CsvService } from 'angular2-json2csv';
import { ViewChild } from '@angular/core';
import * as _ from 'underscore';
import * as moment from 'moment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  fileStudent: any;
  fileParent: any;
  fileSibling: any;
  fileStudentContact: any;
  fileCMSContact: any;
  fileCMSStaff: any;
  fdStu;
  fdCon;
  fdSib;
  fdSc;
  fdCms;
  fdStaff;

  consolidatedResult;
  result = [];

  progressValues = {
    step1: {
      'value': 25,
      'label': 'Syncing Contacts records with siblings'
    },
    step2: {
      'value': 50,
      'label': 'Syncing Student records with siblings'
    },
    step3: {
      'value': 75,
      'label': 'Generating Student Contact map'
    },
    step4: {
      'value': 100,
      'label': 'Competed!'
    }
  };

  pLabel = 'Normalizing contacts'; // default
  pValue = 100;
  pType = 'info';

  showProgress = false;

  @ViewChild('fInputS') fInputS: any;
  @ViewChild('fInputP') fInputP: any;
  @ViewChild('fInputSB') fInputSB: any;
  @ViewChild('fInputSC') fInputSC: any;
  @ViewChild('fInputCms') fInputCms: any;
  @ViewChild('fInputStaff') fInputStaff: any;


  constructor(private _csvService: CsvService, public zone: NgZone) { }

  readFile = (e, t) => {

    if (t == 'stu') {
      this.fileStudent = e.target.files[0];
      this.fileStudent.ft = 'stu';
    }
    else if (t == 'par') {
      this.fileParent = e.target.files[0];
      this.fileParent.ft = 'par';
    }
    else {
      this.fileCMSStaff = e.target.files[0];
      this.fileCMSStaff.ft = 'cmsStaff';
      this.fileCMSContact = e.target.files[1];
      this.fileCMSContact.ft = 'cmsMaster';
    }

  };
  files = [];
  convertFile() {
    this.showProgress = true;
    if (this.fileStudent || this.fileParent) {
      this.files.push(this.fileStudent);
      this.files.push(this.fileParent);
      this.readFileContent(this.files);
    }
    if (this.fileCMSContact) {
      this.files.push(this.fileCMSStaff);
      this.files.push(this.fileCMSContact)
      this.readFileContent(this.files);
    }
  }

  readFileContent(files) {
    for (let i = 0; i < files.length; i++) {
      let fileReader = new FileReader();
      fileReader.onloadend = (function (f, c) {
        return function () {
          if (files[i].ft == 'stu') {
            c.fdStu = c.csvToJSON(fileReader.result);
          }
          if (files[i].ft == 'par') {
            c.fdCon = c.csvToJSON(fileReader.result);
          }
          if (files[i].ft == 'cmsMaster') {
            c.fdCms = c.csvToJSON(fileReader.result);
          }
          if (files[i].ft == 'cmsStaff') {
            c.fdStaff = c.csvToJSON(fileReader.result);
          }
          if (c.fdStu && c.fdCon) {
            c.processJSON();
          }
        };
      })(files[i], this);
      fileReader.readAsText(files[i]);
    }

    if (this.fdCms && this.fdStaff) {
      this.processCmsJSON();
    }

  }

  // Return array of string values, or NULL if CSV string not well formed.
  CSVtoArray(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    // if (!re_valid.test(text)) return null;
    var a = [];                     // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
      function (m0, m1, m2, m3) {
        // Remove backslash from \' in single quoted values.
        if (m1 !== undefined) a.push(m1.replace(/\\'/g, '\''));
        // Remove backslash from \" in double quoted values.
        else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
        else if (m3 !== undefined) a.push(m3);
        return ''; // Return empty string.
      });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
  };

  csvToJSON(csv) {
    debugger;
    let lines = csv.split('\n');
    let result = [];
    let headers = this.CSVtoArray(lines[0]);

    for (let i = 1; i < lines.length - 1; i++) {
      let obj = {};
      let cL = this.CSVtoArray(lines[i]);
      for (let j = 0; j < headers.length; j++) {
        obj[headers[j]] = cL[j];
      }
      result.push(obj);
    }
    return result; //JSON
  }

  processJSON() {
    // actual business logic
    let conResult = [];
    for (let j = 0; j < this.fdCon.length; j++) {
      for (let i = 0; i < this.fdStu.length; i++) {
        let cType = this.fdCon[j].contact_id.substr(1);
        let cType_first = this.fdCon[j].contact_id.substr(0, 1);
        if (this.fdStu[i].upn == cType && cType_first !== 'G') {
          if ((this.fdStu[i].addline1.localeCompare(this.fdCon[j].addline1) === 0 && this.fdStu[i].addline2.localeCompare(this.fdCon[j].addline2) === 0
            && this.fdStu[i].addline3.localeCompare(this.fdCon[j].addline3) === 0 && this.fdStu[i].addline4.localeCompare(this.fdCon[j].addline4 === 0)
            && this.fdStu[i].addline5.localeCompare(this.fdCon[j].addline5) === 0 && this.fdStu[i].postcode.localeCompare(this.fdCon[j].postcode === 0))) {
            this.fdStu[i].flag = true;
          } else {
            this.fdStu[i].flag = false;
          }
          conResult.push(this.fdStu[i]);
        }
      }
    }
    this._csvService.download(conResult, /(.+?)(\.[^.]*$|$)/.exec(this.fileStudent.name)[1] + '-output.csv');
    this.reset();
  }

  processCmsStaffJSON() {
    // actual business logic
    let staff = this.createStaff(this.fdStaff);
    // download the result file (JsonToCSV)
    this._csvService.download(staff, /(.+?)(\.[^.]*$|$)/.exec('STAFF_1119999')[1]);
    this.reset();
  }

  processCmsJSON() {
    debugger;
    // actual business logic
    console.log(this.fdCms);
    console.log(this.fdStaff);
    let studentCore = this.createStudentCore(this.fdCms);
   // let contact = this.createContactInfo(this.fdCms, this.fdStaff);
    let studentContact = this.createStudentContact(this.fdCms, this.fdStaff);
    let studentAdditional = this.createStudentAdditional(studentCore);
    let studentMember = this.createStudentMember(studentCore);
    let studentDoaDol = this.createStudentDoaDol(studentCore);
    let studentTravelMode = this.createStudentTravelMode(studentCore);
    // download the result file (JsonToCSV)

    this._csvService.download(studentCore, /(.+?)(\.[^.]*$|$)/.exec('STUDENT_CORE_1119999')[1]);
   // this._csvService.download(contact, /(.+?)(\.[^.]*$|$)/.exec('CONTACT_1119999')[1]);
    this._csvService.download(studentContact, /(.+?)(\.[^.]*$|$)/.exec('STUDENT_CONTACT_1119999')[1]);
    this._csvService.download(studentAdditional, /(.+?)(\.[^.]*$|$)/.exec('STUDENT_ADDITIONAL_1119999')[1]);
    this._csvService.download(studentMember, /(.+?)(\.[^.]*$|$)/.exec('STUDENT_MEMBER_1119999')[1]);
    this._csvService.download(studentDoaDol, /(.+?)(\.[^.]*$|$)/.exec('STUDENT_DOA_DOL_1119999')[1]);
    this._csvService.download(studentTravelMode, /(.+?)(\.[^.]*$|$)/.exec('STUDENT_TRAVEL_MODE_1119999')[1]);

    this.reset();
  }


  createStudentCore(student) {
    let student_core = [];
    for (let j = 0; j < student.length; j++) {
      var studentCore = {
        surname: this.fdCms[j].surname ? this.fdCms[j].surname : "",
        forename: this.fdCms[j].forename ? this.fdCms[j].forename : "",
        midname: this.fdCms[j].midname ? this.fdCms[j].midname : "",
        pref_surname: this.fdCms[j].pref_surname ? this.fdCms[j].pref_surname : "",
        pref_forename: this.fdCms[j].pref_forename ? this.fdCms[j].pref_forename : "",
        //dateformat dd/mm/yyyy and all the cars are camcel casing
        dob: this.fdCms[j].dob ? this.parseDate(this.fdCms[j].dob) : "",
        gender: this.fdCms[j].gender ? this.fdCms[j].gender : "",
        uln: this.fdCms[j].uln ? this.fdCms[j].uln : "",
        Year_group: this.fdCms[j].Year_group ? this.camelCasingString(this.fdCms[j].Year_group) : "",
        ethnic_group: this.fdCms[j].ethnic_group ? this.fdCms[j].ethnic_group : "",
        ethnicity_source: this.fdCms[j].ethnicity_source ? this.fdCms[j].ethnicity_source : "",
        first_language: this.fdCms[j].Mother_Tongue ? this.fdCms[j].Mother_Tongue : "",
        phone1: this.fdCms[j].phone1 ? this.fdCms[j].phone1 : "",
        phone1_type: this.fdCms[j].phone1_type ? this.fdCms[j].phone1_type : "",
        phone2: this.fdCms[j].phone2 ? this.fdCms[j].phone2 : "",
        phone2_type: this.fdCms[j].phone2_type ? this.fdCms[j].phone2_type : "",
        email: this.fdCms[j].email ? this.fdCms[j].email : "",
        apartment: this.fdCms[j].apartment ? this.fdCms[j].apartment : "",
        house_name: this.fdCms[j].house_name ? this.fdCms[j].house_name : "",
        house_number: this.fdCms[j].house_number ? this.fdCms[j].house_number : "",
        street: this.fdCms[j].street ? this.fdCms[j].street : "",
        district: this.fdCms[j].district ? this.fdCms[j].district : "",
        town: this.fdCms[j].town ? this.fdCms[j].town : "",
        county: this.fdCms[j].county ? this.fdCms[j].county : "",
        country: this.fdCms[j].country ? this.fdCms[j].country : "",
        addline1: this.fdCms[j].addline1 ? this.fdCms[j].addline1 : "",
        addline2: this.fdCms[j].addline2 ? this.fdCms[j].addline2 : "",
        addline3: this.fdCms[j].addline3 ? this.fdCms[j].addline3 : "",
        addline4: this.fdCms[j].addline4 ? this.fdCms[j].addline4 : "",
        addline5: this.fdCms[j].addline5 ? this.fdCms[j].addline5 : "",
        postcode: this.fdCms[j].postcode ? this.fdCms[j].postcode : "",
        sen_status: this.fdCms[j].sen_status ? this.fdCms[j].sen_status : "",
        sen_start_date: this.fdCms[j].sen_start_date ? this.fdCms[j].sen_start_date : "",
        upn: this.fdCms[j].upn
      }
      student_core.push(studentCore);
    }

    return student_core;
  }

  createContactInfo(fdCms, fdStaff) {
    let contact_info = [];
    let upnOfStaffKids = [];
    for (let j = 0; j < fdCms.length; j++) {

      for (let i = 0; i < fdStaff.length; i++) {
        if (fdStaff[i].Employee_Code === fdCms[j].if_Y_Emp_id_of_staff
          || fdStaff[i].Employee_Code === fdCms[j].if_Y_Campus_Emp_id_of_staff) {
          upnOfStaffKids.push(fdCms[j].upn);
          if (fdStaff[i].Employee_Gender == "Female") {
            var contact_F = {
              title: fdCms[j].ftitle ? fdCms[j].ftitle : "",
              forename: fdCms[j].fforename ? fdCms[j].fforename : "",
              surname: fdCms[j].fsurname ? fdCms[j].fsurname : "",
              gender: fdCms[j].fgender ? fdCms[j].fgender : "",
              apartment: "",
              house_name: fdCms[j].fhouse_name ? fdCms[j].fhouse_name : "",
              house_number: fdCms[j].fhouse_number ? fdCms[j].fhouse_number : "",
              street: fdCms[j].fstreet ? fdCms[j].fstreet : "",
              district: fdCms[j].fdistrict ? fdCms[j].fdistrict : "",
              town: fdCms[j].ftown ? fdCms[j].ftown : "",
              county: fdCms[j].fcounty ? fdCms[j].fcounty : "",
              country: fdCms[j].fcountry ? fdCms[j].fcountry : "",
              addline1: fdCms[j].addline1 ? fdCms[j].addline1 : "",
              addline2: fdCms[j].addline2 ? fdCms[j].addline2 : "",
              addline3: fdCms[j].addline3 ? fdCms[j].addline3 : "",
              addline4: fdCms[j].addline4 ? fdCms[j].addline4 : "",
              addline5: fdCms[j].addline5 ? fdCms[j].addline5 : "",
              postcode: fdCms[j].postcode ? fdCms[j].postcode : "",
              home_phone: fdCms[j].fhome_phone ? fdCms[j].fhome_phone : "",
              work_phone: fdCms[j].fwork_phone ? fdCms[j].fwork_phone : "",
              mobile: fdCms[j].fmobile ? fdCms[j].fmobile : "",
              main_phone: fdCms[j].fmain_phone ? fdCms[j].fmain_phone : "",
              home_email: fdCms[j].fhome_email ? fdCms[j].fhome_email : "",
              work_email: fdCms[j].fwork_email ? fdCms[j].fwork_email : "",
              sif_ref_id: fdCms[j].fsif_ref_id ? fdCms[j].fsif_ref_id : "",
              // bill_payer_reference: "F" + fdCms[j].upn.split("/")[0] + fdCms[j].upn.split("/")[1],
              fax_number: fdCms[j].ffax_number ? fdCms[j].ffax_number : "",
              occupation_type: fdCms[j].foccupation_type ? fdCms[j].foccupation_type : "",
              job_title: fdCms[j].fFather_Designation ? fdCms[j].fFather_Designation : "",
              place_of_work: fdCms[j].fFather_Organization ? fdCms[j].fFather_Organization : "",
              contact_id: "F" + fdCms[j].upn
            }
            var contact_M = {
              title: fdCms[j].mtitle ? fdCms[j].mtitle : "",
              forename: fdCms[j].mforename ? fdCms[j].mforename : "",
              surname: fdCms[j].msurname ? fdCms[j].msurname : "",
              gender: fdCms[j].mgender ? fdCms[j].mgender : "",
              apartment: "",
              house_name: fdCms[j].mhouse_name ? fdCms[j].mhouse_name : "",
              house_number: fdCms[j].mhouse_number ? fdCms[j].mhouse_number : "",
              street: fdCms[j].mstreet ? fdCms[j].mstreet : "",
              district: fdCms[j].mdistrict ? fdCms[j].mdistrict : "",
              town: fdCms[j].mtown ? fdCms[j].mtown : "",
              county: fdCms[j].mcounty ? fdCms[j].mcounty : "",
              country: fdCms[j].mcountry ? fdCms[j].mcountry : "",
              addline1: fdCms[j].addline1 ? fdCms[j].addline1 : "",
              addline2: fdCms[j].addline2 ? fdCms[j].addline2 : "",
              addline3: fdCms[j].addline3 ? fdCms[j].addline3 : "",
              addline4: fdCms[j].addline4 ? fdCms[j].addline4 : "",
              addline5: fdCms[j].addline5 ? fdCms[j].addline5 : "",
              postcode: fdCms[j].postcode ? fdCms[j].postcode : "",
              home_phone: fdCms[j].mhome_phone ? fdCms[j].mhome_phone : "",
              work_phone: fdCms[j].mwork_phone ? fdCms[j].mwork_phone : "",
              mobile: fdCms[j].mmobile ? fdCms[j].mmobile : "",
              main_phone: fdCms[j].mmain_phone ? fdCms[j].mmain_phone : "",
              home_email: fdCms[j].mhome_email ? fdCms[j].mhome_email : "",
              work_email: fdCms[j].mwork_email ? fdCms[j].mwork_email : "",
              sif_ref_id: fdCms[j].msif_ref_id ? fdCms[j].msif_ref_id : "",
              //  bill_payer_reference: fdCms[j].mbill_payer_reference ? fdCms[j].mbill_payer_reference : "",
              fax_number: fdCms[j].mfax_number ? fdCms[j].mfax_number : "",
              occupation_type: fdCms[j].moccupation_type ? fdCms[j].moccupation_type : "",
              job_title: fdCms[j].mMother_Designation ? fdCms[j].mMother_Designation : "",
              place_of_work: fdCms[j].mMother_Organization ? fdCms[j].mMother_Organization : "",
              contact_id: "" + fdStaff[i].Employee_Code
            }
            var contact_G = {
              title: fdCms[j].gtitle ? fdCms[j].gtitle : "",
              forename: fdCms[j].gforename ? fdCms[j].gforename : "",
              surname: fdCms[j].gsurname ? fdCms[j].gsurname : "",
              gender: fdCms[j].ggender ? fdCms[j].ggender : "",
              apartment: "",
              house_name: fdCms[j].ghouse_name ? fdCms[j].ghouse_name : "",
              house_number: fdCms[j].ghouse_number ? fdCms[j].ghouse_number : "",
              street: fdCms[j].gstreet ? fdCms[j].gstreet : "",
              district: fdCms[j].gdistrict ? fdCms[j].gdistrict : "",
              town: fdCms[j].gtown ? fdCms[j].gtown : "",
              county: fdCms[j].gcounty ? fdCms[j].gcounty : "",
              country: fdCms[j].gcountry ? fdCms[j].gcountry : "",
              addline1: fdCms[j].gaddline1 ? fdCms[j].gaddline1 : "",
              addline2: fdCms[j].gaddline2 ? fdCms[j].gaddline2 : "",
              addline3: fdCms[j].gaddline3 ? fdCms[j].gaddline3 : "",
              addline4: fdCms[j].gaddline4 ? fdCms[j].gaddline4 : "",
              addline5: fdCms[j].gaddline5 ? fdCms[j].gaddline5 : "",
              postcode: fdCms[j].gpostcode ? fdCms[j].gpostcode : "",
              home_phone: fdCms[j].ghome_phone ? fdCms[j].ghome_phone : "",
              work_phone: fdCms[j].gwork_phone ? fdCms[j].gwork_phone : "",
              mobile: fdCms[j].gmobile ? fdCms[j].gmobile : "",
              main_phone: fdCms[j].gmain_phone ? fdCms[j].gmain_phone : "",
              home_email: fdCms[j].ghome_email ? fdCms[j].ghome_email : "",
              work_email: fdCms[j].gwork_email ? fdCms[j].gwork_email : "",
              sif_ref_id: "",
              //  bill_payer_reference: fdCms[j].gbill_payer_reference ? fdCms[j].gbill_payer_reference : "",
              fax_number: fdCms[j].gfax_number ? fdCms[j].gfax_number : "",
              occupation_type: fdCms[j].goccupation_type ? fdCms[j].goccupation_type : "",
              job_title: fdCms[j].gjob_title ? fdCms[j].gjob_title : "",
              place_of_work: fdCms[j].gplace_of_work ? fdCms[j].gplace_of_work : "",
              contact_id: "G" + fdCms[j].upn
            }
            contact_info.push(contact_F);
            contact_info.push(contact_M);

            //if guardian is available then only create the row
            if (fdCms[j].ggender !== "") {
              contact_info.push(contact_G);
            }
          }
          if (fdStaff[i].Employee_Gender == "Male") {

            var contact_F = {
              title: fdCms[j].ftitle ? fdCms[j].ftitle : "",
              forename: fdCms[j].fforename ? fdCms[j].fforename : "",
              surname: fdCms[j].fsurname ? fdCms[j].fsurname : "",
              gender: fdCms[j].fgender ? fdCms[j].fgender : "",
              apartment: "",
              house_name: fdCms[j].fhouse_name ? fdCms[j].fhouse_name : "",
              house_number: fdCms[j].fhouse_number ? fdCms[j].fhouse_number : "",
              street: fdCms[j].fstreet ? fdCms[j].fstreet : "",
              district: fdCms[j].fdistrict ? fdCms[j].fdistrict : "",
              town: fdCms[j].ftown ? fdCms[j].ftown : "",
              county: fdCms[j].fcounty ? fdCms[j].fcounty : "",
              country: fdCms[j].fcountry ? fdCms[j].fcountry : "",
              addline1: fdCms[j].addline1 ? fdCms[j].addline1 : "",
              addline2: fdCms[j].addline2 ? fdCms[j].addline2 : "",
              addline3: fdCms[j].addline3 ? fdCms[j].addline3 : "",
              addline4: fdCms[j].addline4 ? fdCms[j].addline4 : "",
              addline5: fdCms[j].addline5 ? fdCms[j].addline5 : "",
              postcode: fdCms[j].postcode ? fdCms[j].postcode : "",
              home_phone: fdCms[j].fhome_phone ? fdCms[j].fhome_phone : "",
              work_phone: fdCms[j].fwork_phone ? fdCms[j].fwork_phone : "",
              mobile: fdCms[j].fmobile ? fdCms[j].fmobile : "",
              main_phone: fdCms[j].fmain_phone ? fdCms[j].fmain_phone : "",
              home_email: fdCms[j].fhome_email ? fdCms[j].fhome_email : "",
              work_email: fdCms[j].fwork_email ? fdCms[j].fwork_email : "",
              sif_ref_id: fdCms[j].fsif_ref_id ? fdCms[j].fsif_ref_id : "",
              // bill_payer_reference: "F" + fdCms[j].upn.split("/")[0] + fdCms[j].upn.split("/")[1],
              fax_number: fdCms[j].ffax_number ? fdCms[j].ffax_number : "",
              occupation_type: fdCms[j].foccupation_type ? fdCms[j].foccupation_type : "",
              job_title: fdCms[j].fFather_Designation ? fdCms[j].fFather_Designation : "",
              place_of_work: fdCms[j].fFather_Organization ? fdCms[j].fFather_Organization : "",
              contact_id: "" + fdStaff[i].Employee_Code
            }
            var contact_M = {
              title: fdCms[j].mtitle ? fdCms[j].mtitle : "",
              forename: fdCms[j].mforename ? fdCms[j].mforename : "",
              surname: fdCms[j].msurname ? fdCms[j].msurname : "",
              gender: fdCms[j].mgender ? fdCms[j].mgender : "",
              apartment: "",
              house_name: fdCms[j].mhouse_name ? fdCms[j].mhouse_name : "",
              house_number: fdCms[j].mhouse_number ? fdCms[j].mhouse_number : "",
              street: fdCms[j].mstreet ? fdCms[j].mstreet : "",
              district: fdCms[j].mdistrict ? fdCms[j].mdistrict : "",
              town: fdCms[j].mtown ? fdCms[j].mtown : "",
              county: fdCms[j].mcounty ? fdCms[j].mcounty : "",
              country: fdCms[j].mcountry ? fdCms[j].mcountry : "",
              addline1: fdCms[j].addline1 ? fdCms[j].addline1 : "",
              addline2: fdCms[j].addline2 ? fdCms[j].addline2 : "",
              addline3: fdCms[j].addline3 ? fdCms[j].addline3 : "",
              addline4: fdCms[j].addline4 ? fdCms[j].addline4 : "",
              addline5: fdCms[j].addline5 ? fdCms[j].addline5 : "",
              postcode: fdCms[j].postcode ? fdCms[j].postcode : "",
              home_phone: fdCms[j].mhome_phone ? fdCms[j].mhome_phone : "",
              work_phone: fdCms[j].mwork_phone ? fdCms[j].mwork_phone : "",
              mobile: fdCms[j].mmobile ? fdCms[j].mmobile : "",
              main_phone: fdCms[j].mmain_phone ? fdCms[j].mmain_phone : "",
              home_email: fdCms[j].mhome_email ? fdCms[j].mhome_email : "",
              work_email: fdCms[j].mwork_email ? fdCms[j].mwork_email : "",
              sif_ref_id: fdCms[j].msif_ref_id ? fdCms[j].msif_ref_id : "",
              //  bill_payer_reference: fdCms[j].mbill_payer_reference ? fdCms[j].mbill_payer_reference : "",
              fax_number: fdCms[j].mfax_number ? fdCms[j].mfax_number : "",
              occupation_type: fdCms[j].moccupation_type ? fdCms[j].moccupation_type : "",
              job_title: fdCms[j].mMother_Designation ? fdCms[j].mMother_Designation : "",
              place_of_work: fdCms[j].mMother_Organization ? fdCms[j].mMother_Organization : "",
              contact_id: "M" + fdCms[j].upn
            }
            var contact_G = {
              title: fdCms[j].gtitle ? fdCms[j].gtitle : "",
              forename: fdCms[j].gforename ? fdCms[j].gforename : "",
              surname: fdCms[j].gsurname ? fdCms[j].gsurname : "",
              gender: fdCms[j].ggender ? fdCms[j].ggender : "",
              apartment: "",
              house_name: fdCms[j].ghouse_name ? fdCms[j].ghouse_name : "",
              house_number: fdCms[j].ghouse_number ? fdCms[j].ghouse_number : "",
              street: fdCms[j].gstreet ? fdCms[j].gstreet : "",
              district: fdCms[j].gdistrict ? fdCms[j].gdistrict : "",
              town: fdCms[j].gtown ? fdCms[j].gtown : "",
              county: fdCms[j].gcounty ? fdCms[j].gcounty : "",
              country: fdCms[j].gcountry ? fdCms[j].gcountry : "",
              addline1: fdCms[j].gaddline1 ? fdCms[j].gaddline1 : "",
              addline2: fdCms[j].gaddline2 ? fdCms[j].gaddline2 : "",
              addline3: fdCms[j].gaddline3 ? fdCms[j].gaddline3 : "",
              addline4: fdCms[j].gaddline4 ? fdCms[j].gaddline4 : "",
              addline5: fdCms[j].gaddline5 ? fdCms[j].gaddline5 : "",
              postcode: fdCms[j].gpostcode ? fdCms[j].gpostcode : "",
              home_phone: fdCms[j].ghome_phone ? fdCms[j].ghome_phone : "",
              work_phone: fdCms[j].gwork_phone ? fdCms[j].gwork_phone : "",
              mobile: fdCms[j].gmobile ? fdCms[j].gmobile : "",
              main_phone: fdCms[j].gmain_phone ? fdCms[j].gmain_phone : "",
              home_email: fdCms[j].ghome_email ? fdCms[j].ghome_email : "",
              work_email: fdCms[j].gwork_email ? fdCms[j].gwork_email : "",
              sif_ref_id: "",
              //  bill_payer_reference: fdCms[j].gbill_payer_reference ? fdCms[j].gbill_payer_reference : "",
              fax_number: fdCms[j].gfax_number ? fdCms[j].gfax_number : "",
              occupation_type: fdCms[j].goccupation_type ? fdCms[j].goccupation_type : "",
              job_title: fdCms[j].gjob_title ? fdCms[j].gjob_title : "",
              place_of_work: fdCms[j].gplace_of_work ? fdCms[j].gplace_of_work : "",
              contact_id: "G" + fdCms[j].upn
            }

            contact_info.push(contact_F);
            contact_info.push(contact_M);

            //if guardian is available then only create the row
            if (fdCms[j].ggender !== "") {
              contact_info.push(contact_G);
            }
          }

        }

      }
      // Father and mother is going to be copied the address of Student.
      if (!_.contains(upnOfStaffKids, fdCms[j].upn)) {
        var contact_F = {
          title: fdCms[j].ftitle ? fdCms[j].ftitle : "",
          forename: fdCms[j].fforename ? fdCms[j].fforename : "",
          surname: fdCms[j].fsurname ? fdCms[j].fsurname : "",
          gender: fdCms[j].fgender ? fdCms[j].fgender : "",
          apartment: "",
          house_name: fdCms[j].fhouse_name ? fdCms[j].fhouse_name : "",
          house_number: fdCms[j].fhouse_number ? fdCms[j].fhouse_number : "",
          street: fdCms[j].fstreet ? fdCms[j].fstreet : "",
          district: fdCms[j].fdistrict ? fdCms[j].fdistrict : "",
          town: fdCms[j].ftown ? fdCms[j].ftown : "",
          county: fdCms[j].fcounty ? fdCms[j].fcounty : "",
          country: fdCms[j].fcountry ? fdCms[j].fcountry : "",
          addline1: fdCms[j].addline1 ? fdCms[j].addline1 : "",
          addline2: fdCms[j].addline2 ? fdCms[j].addline2 : "",
          addline3: fdCms[j].addline3 ? fdCms[j].addline3 : "",
          addline4: fdCms[j].addline4 ? fdCms[j].addline4 : "",
          addline5: fdCms[j].addline5 ? fdCms[j].addline5 : "",
          postcode: fdCms[j].postcode ? fdCms[j].postcode : "",
          home_phone: fdCms[j].fhome_phone ? fdCms[j].fhome_phone : "",
          work_phone: fdCms[j].fwork_phone ? fdCms[j].fwork_phone : "",
          mobile: fdCms[j].fmobile ? fdCms[j].fmobile : "",
          main_phone: fdCms[j].fmain_phone ? fdCms[j].fmain_phone : "",
          home_email: fdCms[j].fhome_email ? fdCms[j].fhome_email : "",
          work_email: fdCms[j].fwork_email ? fdCms[j].fwork_email : "",
          sif_ref_id: fdCms[j].fsif_ref_id ? fdCms[j].fsif_ref_id : "",
          // bill_payer_reference: "F" + fdCms[j].upn.split("/")[0] + fdCms[j].upn.split("/")[1],
          fax_number: fdCms[j].ffax_number ? fdCms[j].ffax_number : "",
          occupation_type: fdCms[j].foccupation_type ? fdCms[j].foccupation_type : "",
          job_title: fdCms[j].fFather_Designation ? fdCms[j].fFather_Designation : "",
          place_of_work: fdCms[j].fFather_Organization ? fdCms[j].fFather_Organization : "",
          contact_id: "F" + fdCms[j].upn
        }
        var contact_M = {
          title: fdCms[j].mtitle ? fdCms[j].mtitle : "",
          forename: fdCms[j].mforename ? fdCms[j].mforename : "",
          surname: fdCms[j].msurname ? fdCms[j].msurname : "",
          gender: fdCms[j].mgender ? fdCms[j].mgender : "",
          apartment: "",
          house_name: fdCms[j].mhouse_name ? fdCms[j].mhouse_name : "",
          house_number: fdCms[j].mhouse_number ? fdCms[j].mhouse_number : "",
          street: fdCms[j].mstreet ? fdCms[j].mstreet : "",
          district: fdCms[j].mdistrict ? fdCms[j].mdistrict : "",
          town: fdCms[j].mtown ? fdCms[j].mtown : "",
          county: fdCms[j].mcounty ? fdCms[j].mcounty : "",
          country: fdCms[j].mcountry ? fdCms[j].mcountry : "",
          addline1: fdCms[j].addline1 ? fdCms[j].addline1 : "",
          addline2: fdCms[j].addline2 ? fdCms[j].addline2 : "",
          addline3: fdCms[j].addline3 ? fdCms[j].addline3 : "",
          addline4: fdCms[j].addline4 ? fdCms[j].addline4 : "",
          addline5: fdCms[j].addline5 ? fdCms[j].addline5 : "",
          postcode: fdCms[j].postcode ? fdCms[j].postcode : "",
          home_phone: fdCms[j].mhome_phone ? fdCms[j].mhome_phone : "",
          work_phone: fdCms[j].mwork_phone ? fdCms[j].mwork_phone : "",
          mobile: fdCms[j].mmobile ? fdCms[j].mmobile : "",
          main_phone: fdCms[j].mmain_phone ? fdCms[j].mmain_phone : "",
          home_email: fdCms[j].mhome_email ? fdCms[j].mhome_email : "",
          work_email: fdCms[j].mwork_email ? fdCms[j].mwork_email : "",
          sif_ref_id: fdCms[j].msif_ref_id ? fdCms[j].msif_ref_id : "",
          //  bill_payer_reference: fdCms[j].mbill_payer_reference ? fdCms[j].mbill_payer_reference : "",
          fax_number: fdCms[j].mfax_number ? fdCms[j].mfax_number : "",
          occupation_type: fdCms[j].moccupation_type ? fdCms[j].moccupation_type : "",
          job_title: fdCms[j].mMother_Designation ? fdCms[j].mMother_Designation : "",
          place_of_work: fdCms[j].mMother_Organization ? fdCms[j].mMother_Organization : "",
          contact_id: "M" + fdCms[j].upn
        }
        var contact_G = {
          title: fdCms[j].gtitle ? fdCms[j].gtitle : "",
          forename: fdCms[j].gforename ? fdCms[j].gforename : "",
          surname: fdCms[j].gsurname ? fdCms[j].gsurname : "",
          gender: fdCms[j].ggender ? fdCms[j].ggender : "",
          apartment: "",
          house_name: fdCms[j].ghouse_name ? fdCms[j].ghouse_name : "",
          house_number: fdCms[j].ghouse_number ? fdCms[j].ghouse_number : "",
          street: fdCms[j].gstreet ? fdCms[j].gstreet : "",
          district: fdCms[j].gdistrict ? fdCms[j].gdistrict : "",
          town: fdCms[j].gtown ? fdCms[j].gtown : "",
          county: fdCms[j].gcounty ? fdCms[j].gcounty : "",
          country: fdCms[j].gcountry ? fdCms[j].gcountry : "",
          addline1: fdCms[j].gaddline1 ? fdCms[j].gaddline1 : "",
          addline2: fdCms[j].gaddline2 ? fdCms[j].gaddline2 : "",
          addline3: fdCms[j].gaddline3 ? fdCms[j].gaddline3 : "",
          addline4: fdCms[j].gaddline4 ? fdCms[j].gaddline4 : "",
          addline5: fdCms[j].gaddline5 ? fdCms[j].gaddline5 : "",
          postcode: fdCms[j].gpostcode ? fdCms[j].gpostcode : "",
          home_phone: fdCms[j].ghome_phone ? fdCms[j].ghome_phone : "",
          work_phone: fdCms[j].gwork_phone ? fdCms[j].gwork_phone : "",
          mobile: fdCms[j].gmobile ? fdCms[j].gmobile : "",
          main_phone: fdCms[j].gmain_phone ? fdCms[j].gmain_phone : "",
          home_email: fdCms[j].ghome_email ? fdCms[j].ghome_email : "",
          work_email: fdCms[j].gwork_email ? fdCms[j].gwork_email : "",
          sif_ref_id: "",
          //  bill_payer_reference: fdCms[j].gbill_payer_reference ? fdCms[j].gbill_payer_reference : "",
          fax_number: fdCms[j].gfax_number ? fdCms[j].gfax_number : "",
          occupation_type: fdCms[j].goccupation_type ? fdCms[j].goccupation_type : "",
          job_title: fdCms[j].gjob_title ? fdCms[j].gjob_title : "",
          place_of_work: fdCms[j].gplace_of_work ? fdCms[j].gplace_of_work : "",
          contact_id: "G" + fdCms[j].upn
        }
        contact_info.push(contact_F);
        contact_info.push(contact_M);

        //if guardian is available then only create the row
        if (fdCms[j].ggender !== "") {
          contact_info.push(contact_G);
        }
      }
    }
    return contact_info;
  }



  // need to start

  createStudentContact(fdCms, fdStaff) {
    let student_contact = [];
    let sibling_dups = [];
    debugger;
    for (let s = 0; s < fdCms.length; s++) {

      if (fdCms[s].Sibling_in_the_same_branch === "Y" && (fdCms[s].UPN_of_Sibling1 !== "" ||
        fdCms[s].UPN_of_Sibling2 !== "" || fdCms[s].UPN_of_Sibling3 !== "")) {

        if (!_.contains(sibling_dups, fdCms[s].UPN_of_Sibling1, fdCms[s].UPN_of_Sibling2, fdCms[s].UPN_of_Sibling3)) {
          sibling_dups.push(fdCms[s].UPN_of_Sibling1);
          sibling_dups.push(fdCms[s].UPN_of_Sibling2);
          sibling_dups.push(fdCms[s].UPN_of_Sibling3);


          //If the student if_Y_Emp_id_of_staff and if_Y_Campus_Emp_id_of_staff then iterate the staff file and check with the staff employee code
          // and if both are same then then upn of sibling should be of Fahter/mother employee code.

          if (fdCms[s].if_Y_Emp_id_of_staff || fdCms[s].if_Y_Campus_Emp_id_of_staff) {
            for (let i = 0; i < fdStaff.length; i++) {
              if (fdStaff[i].Employee_Code === fdCms[s].if_Y_Emp_id_of_staff
                || fdStaff[i].Employee_Code === fdCms[s].if_Y_Campus_Emp_id_of_staff) {
                if (fdStaff[i].Employee_Gender == "Female") {

                  if (fdCms[s].UPN_of_Sibling1 !== "") {
                    student_contact.push(this.studentContact_F("M" + fdCms[s].UPN_of_Sibling1, fdCms[s].upn));
                    student_contact.push(this.studentContact_M("" + fdStaff[i].Employee_Code, fdCms[s].upn));
                    if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                      student_contact.push(this.studentContact_G("G" + fdCms[s].UPN_of_Sibling1, fdCms[s].upn));
                    }
                  }
                  if (fdCms[s].UPN_of_Sibling2 !== "") {

                    student_contact.push(this.studentContact_F("M" + fdCms[s].UPN_of_Sibling1, fdCms[s].UPN_of_Sibling2));
                    student_contact.push(this.studentContact_M("" + fdStaff[i].Employee_Code, fdCms[s].UPN_of_Sibling2));
                    if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                      student_contact.push(this.studentContact_G("G" + fdCms[s].UPN_of_Sibling2, fdCms[s].UPN_of_Sibling2));
                    }
                  }
                  if (fdCms[s].UPN_of_Sibling3 !== "") {
                    student_contact.push(this.studentContact_F("M" + fdCms[s].UPN_of_Sibling1, fdCms[s].UPN_of_Sibling3));
                    student_contact.push(this.studentContact_M("" + fdStaff[i].Employee_Code, fdCms[s].UPN_of_Sibling3));
                    if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                      student_contact.push(this.studentContact_G("G" + fdCms[s].UPN_of_Sibling3, fdCms[s].UPN_of_Sibling3));
                    }
                  }
                }
                if (fdStaff[i].Employee_Gender == "Male") {
                  if (fdCms[s].UPN_of_Sibling1 !== "") {
                    student_contact.push(this.studentContact_F("" + fdStaff[i].Employee_Code, fdCms[s].upn));
                    student_contact.push(this.studentContact_M("F" + fdCms[s].UPN_of_Sibling1, fdCms[s].upn));
                    if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                      student_contact.push(this.studentContact_G("G" + fdCms[s].UPN_of_Sibling1, fdCms[s].upn));
                    }
                  }
                  if (fdCms[s].UPN_of_Sibling2 !== "") {

                    student_contact.push(this.studentContact_F("" + fdStaff[i].Employee_Code, fdCms[s].UPN_of_Sibling2));
                    student_contact.push(this.studentContact_M("F" + fdCms[s].UPN_of_Sibling1, fdCms[s].UPN_of_Sibling2));
                    if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                      student_contact.push(this.studentContact_G("G" + fdCms[s].UPN_of_Sibling2, fdCms[s].UPN_of_Sibling2));
                    }
                  }
                  if (fdCms[s].UPN_of_Sibling3 !== "") {

                    student_contact.push(this.studentContact_F("" + fdStaff[i].Employee_Code, fdCms[s].UPN_of_Sibling3));
                    student_contact.push(this.studentContact_M("F" + fdCms[s].UPN_of_Sibling1, fdCms[s].UPN_of_Sibling3));
                    if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                      student_contact.push(this.studentContact_G("G" + fdCms[s].UPN_of_Sibling3, fdCms[s].UPN_of_Sibling3));
                    }
                  }
                }
              }
            }
          } else {

            if (fdCms[s].UPN_of_Sibling1 !== "") {

              student_contact.push(this.studentContact_F("F" + fdCms[s].UPN_of_Sibling1, fdCms[s].upn));
              student_contact.push(this.studentContact_M("M" + fdCms[s].UPN_of_Sibling1, fdCms[s].upn));
              if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                student_contact.push(this.studentContact_G("G" + fdCms[s].UPN_of_Sibling1, fdCms[s].upn));
              }

            }
            if (fdCms[s].UPN_of_Sibling2 !== "") {

              student_contact.push(this.studentContact_F("F" + fdCms[s].UPN_of_Sibling1, fdCms[s].UPN_of_Sibling2));
              student_contact.push(this.studentContact_M("M" + fdCms[s].UPN_of_Sibling1, fdCms[s].UPN_of_Sibling2));
              if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                student_contact.push(this.studentContact_G("G" + fdCms[s].UPN_of_Sibling2, fdCms[s].UPN_of_Sibling2));
              }
            }
            if (fdCms[s].UPN_of_Sibling3 !== "") {

              student_contact.push(this.studentContact_F("F" + fdCms[s].UPN_of_Sibling1, fdCms[s].UPN_of_Sibling3));
              student_contact.push(this.studentContact_M("M" + fdCms[s].UPN_of_Sibling1, fdCms[s].UPN_of_Sibling3));
              if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                student_contact.push(this.studentContact_G("G" + fdCms[s].UPN_of_Sibling2, fdCms[s].UPN_of_Sibling3));
              }


            }
          }
        }

        let scContainer_F = [];
        let scContainer_M = [];
        let scContainer_G = [];
      } else {

        if (fdCms[s].if_Y_Emp_id_of_staff || fdCms[s].if_Y_Campus_Emp_id_of_staff) {
          for (let i = 0; i < fdStaff.length; i++) {
            if (fdStaff[i].Employee_Code === fdCms[s].if_Y_Emp_id_of_staff
              || fdStaff[i].Employee_Code === fdCms[s].if_Y_Campus_Emp_id_of_staff) {

              if (fdStaff[i].Employee_Gender == "Female") {
                student_contact.push(this.studentContact_F("M" + fdCms[s].upn, fdCms[s].upn));
                student_contact.push(this.studentContact_M("" + fdStaff[i].Employee_Code, fdCms[s].upn));
                if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                  student_contact.push(this.studentContact_G("G" + fdCms[s].upn, fdCms[s].upn));
                }
              }
              if (fdStaff[i].Employee_Gender == "Male") {

                student_contact.push(this.studentContact_F("" + fdStaff[i].Employee_Code, fdCms[s].upn));
                student_contact.push(this.studentContact_M("F" + fdCms[s].upn, fdCms[s].upn));
                if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
                  student_contact.push(this.studentContact_G("G" + fdCms[s].upn, fdCms[s].upn));
                }
              }
            }
          }
        }
        else {

          student_contact.push(this.studentContact_F("F" + fdCms[s].upn, fdCms[s].upn));
          student_contact.push(this.studentContact_M("M" + fdCms[s].upn, fdCms[s].upn));
          if (this.fdCms[s].ggender !== "" && this.fdCms[s].gaddline1 !== "") {
            student_contact.push(this.studentContact_G("G" + fdCms[s].upn, fdCms[s].upn));
          }
        }

      }

    }

    return student_contact;
  }



  studentContact_F(contactId, upn) {
    return {
      rel_code: "PAF",
      par_resp: "Y",
      priority: "1",
      priority_source: "P",
      court_order: "",
      correspondence: "Y",
      pupil_report: "Y",
      contact_id: contactId,
      copy_bill: "",
      payment_type: "1",
      bank_name: "",
      bank_account_name: "",
      bank_account_no: "",
      bank_sort_code: "",
      main_bill_payer: "Y",
      upn: upn
    }
  }
  studentContact_M(contactId, upn) {
    return {
      rel_code: "PAM",
      par_resp: "Y",
      priority: "2",
      priority_source: "P",
      court_order: "",
      correspondence: "Y",
      pupil_report: "Y",
      contact_id: contactId,
      copy_bill: "",
      payment_type: "",
      bank_name: "",
      bank_account_name: "",
      bank_account_no: "",
      bank_sort_code: "",
      main_bill_payer: "",
      upn: upn
    }
  }

  studentContact_G(contactId, upn) {
    
   return{
      rel_code: "OTH",
      par_resp: "Y",
      priority: "3",
      priority_source: "P",
      court_order: "",
      correspondence: "Y",
      pupil_report: "Y",
      contact_id: contactId,
      copy_bill: "",
      payment_type: "",
      bank_name: "",
      bank_account_name: "",
      bank_account_no: "",
      bank_sort_code: "",
      main_bill_payer: "",
      upn: upn
    }
  }

  createStudentAdditional(student) {
    let student_addtional = [];
    let i = 1;
    for (let j = 0; j < student.length; j++) {

      var studentAdditional = {
        uci: this.fdCms[j].uci ? this.fdCms[j].uci : "",
        exam_number: this.fdCms[j].exam_number ? this.fdCms[j].exam_number : "",
        adno: i++,
        quick_note: this.fdCms[j].quick_note ? this.fdCms[j].quick_note : "",
        religion_code: this.fdCms[j].Religion ? this.fdCms[j].Religion : "",
        eal_code: this.fdCms[j].eal_code ? this.fdCms[j].eal_code : "",
        service_child: this.fdCms[j].service_child ? this.fdCms[j].service_child : "",
        service_child_source: this.fdCms[j].service_child_source ? this.fdCms[j].service_child_source : "",
        boarder: this.fdCms[j].boarder ? this.fdCms[j].boarder : "",
        YSSA: this.fdCms[j].YSSA ? this.fdCms[j].YSSA : "",
        sif_ref_id: this.fdCms[j].sif_ref_id ? this.fdCms[j].sif_ref_id : "",
        g_and_t_start: this.fdCms[j].g_and_t_start ? this.fdCms[j].g_and_t_start : "",
        g_and_t_end: this.fdCms[j].g_and_t_end ? this.fdCms[j].g_and_t_end : "",
        student_nation: this.fdCms[j].student_nation ? this.fdCms[j].student_nation : "",
        passport_number: this.fdCms[j].passport_number ? this.fdCms[j].passport_number : "",
        passport_issue_date: this.fdCms[j].passport_issue_date ? this.fdCms[j].passport_issue_date : "",
        passport_expiry_date: this.fdCms[j].passport_expiry_date ? this.fdCms[j].passport_expiry_date : "",
        passport_name: this.fdCms[j].passport_name ? this.fdCms[j].passport_name : "",
        visa_number: this.fdCms[j].visa_number ? this.fdCms[j].visa_number : "",
        visa_expiry_date: this.fdCms[j].visa_expiry_date ? this.fdCms[j].visa_expiry_date : "",
        cas_number: this.fdCms[j].cas_number ? this.fdCms[j].cas_number : "",
        pupil_fee_ref: "",
        blood_group: this.fdCms[j].Blood_Group ?
          this.fdCms[j].Blood_Group == 'A+' ? this.fdCms[j].Blood_Group : "" || this.fdCms[j].Blood_Group == 'A-' ? this.fdCms[j].Blood_Group : ""
            || this.fdCms[j].Blood_Group == 'AB+' ? this.fdCms[j].Blood_Group : "" ||
              this.fdCms[j].Blood_Group == 'AB-' ? this.fdCms[j].Blood_Group : "" || this.fdCms[j].Blood_Group == 'O+' ? this.fdCms[j].Blood_Group : ""
                || this.fdCms[j].Blood_Group == 'O-' ? this.fdCms[j].Blood_Group : "" ||
                  this.fdCms[j].Blood_Group == 'B+' ? this.fdCms[j].Blood_Group : "" || this.fdCms[j].Blood_Group == 'B-' ? this.fdCms[j].Blood_Group : "" : "",
        local_upn: this.fdCms[j].upn ? this.fdCms[j].upn : "",
        upn: this.fdCms[j].upn ? this.fdCms[j].upn : "",
      }
      student_addtional.push(studentAdditional);
    }

    return student_addtional;
  }

  createStudentMember(student) {
    let student_member = [];
    for (let j = 0; j < student.length; j++) {
      var studentMember = {
        group_type: "H",
        short_name: this.fdCms[j].House ? this.fdCms[j].House == "UNITY" ? "U" : ""
          || this.fdCms[j].House == "PEACE" ? "P" : "" || this.fdCms[j].House == "HOPE" ? "H" : "" || this.fdCms[j].House == "LOVE" ? "L" : "" : "",
        start_date: "",
        end_date: "",
        upn: this.fdCms[j].upn ? this.fdCms[j].upn : "",
      }
      var studentMember_Y = {
        group_type: "Y",
        short_name: this.fdCms[j].Year_group ? this.fdCms[j].Year_group : "",
        start_date: "",
        end_date: "",
        upn: this.fdCms[j].upn ? this.fdCms[j].upn : "",
      }
      var studentMember_C = {
        group_type: "C",
        short_name: this.fdCms[j].Year_group ? this.fdCms[j].Year_group == "PG" ? "E2" : ""
          || this.fdCms[j].Year_group == "MONT" ? "N1" : "" || this.fdCms[j].Year_group == "NUR" ? "N2" : ""
            || this.fdCms[j].Year_group == "KG" ? "R" : "" || this.fdCms[j].Year_group == "I" ? "1" : ""
              || this.fdCms[j].Year_group == "II" ? "2" : "" || this.fdCms[j].Year_group == "III" ? "3" : ""
                || this.fdCms[j].Year_group == "IV" ? "4" : "" || this.fdCms[j].Year_group == "V" ? "5" : ""
                  || this.fdCms[j].Year_group == "VI" ? "6" : "" || this.fdCms[j].Year_group == "VII" ? "7" : ""
                    || this.fdCms[j].Year_group == "VIII" ? "8" : "" : "",
        start_date: "",
        end_date: "",
        upn: this.fdCms[j].upn ? this.fdCms[j].upn : "",
      }
      var studentMember_R = {
        group_type: "R",
        short_name: this.fdCms[j].Year_group + " " + this.fdCms[j].Section,
        start_date: "",
        end_date: "",
        upn: this.fdCms[j].upn ? this.fdCms[j].upn : "",
      }
      student_member.push(studentMember);
      student_member.push(studentMember_Y);
      student_member.push(studentMember_C);
      student_member.push(studentMember_R);
    }

    return student_member;
  }

  createStudentDoaDol(student) {
    let student_Doa_Dol = [];
    for (let j = 0; j < student.length; j++) {

      let adDate = moment(this.fdCms[j].admission_Date).format("DD/MM/YYYY");
      // change the date to dd/mm/yyyy format
      var studentDoaDol = {
        doa: this.fdCms[j].admission_Date ? this.parseDate(this.fdCms[j].admission_Date) : "",
        dol: "",
        upn: this.fdCms[j].upn ? this.fdCms[j].upn : ""
      }
      student_Doa_Dol.push(studentDoaDol);
    }
    return student_Doa_Dol;
  }

  createStudentTravelMode(student) {
    let student_Travel_Mode = [];
    for (let j = 0; j < student.length; j++) {
      var studentTravelMode = {
        travel_code: this.fdCms[j].Mode_Of_Transport ? this.fdCms[j].Mode_Of_Transport : "",
        start_date: "",
        end_date: "",
        upn: this.fdCms[j].upn ? this.fdCms[j].upn : ""
      }
      student_Travel_Mode.push(studentTravelMode);
    }
    return student_Travel_Mode;
  }

  createStaff(staff) {
    let staff_details = [];
    let staff1 = [];
    for (let j = 0; j < staff.length; j++) {
      debugger;
      if (this.fdStaff[j].Employee_forename !== "") {

        var staffDetails = {
          title: this.fdStaff[j].title ? this.fdStaff[j].title : "",
          forename1: this.fdStaff[j].Employee_forename ? this.fdStaff[j].Employee_forename : "",
          forename2: this.fdStaff[j].Employee_forename2 ? this.fdStaff[j].Employee_forename2 : "",
          forename3: this.fdStaff[j].Employee_forename3 ? this.fdStaff[j].Employee_forename3 : "",
          surname: this.fdStaff[j].Employee_surname ? this.fdStaff[j].Employee_surname : "",
          tt_code: this.fdStaff[j].tt_code ? this.fdStaff[j].tt_code : "",
          payroll_number: this.fdStaff[j].Employee_Code ? this.fdStaff[j].Employee_Code : "",
          curric_role: this.fdStaff[j].curric_role ? this.fdStaff[j].curric_role : "",
          teacher_category: this.fdStaff[j].teacher_category ? this.fdStaff[j].teacher_category : "",
          cover: this.fdStaff[j].cover ? this.fdStaff[j].cover : "",
          gender: this.fdStaff[j].Employee_Gender ? this.fdStaff[j].Employee_Gender : "",
          dob: this.fdStaff[j].Date_Of_Birth ? this.fdStaff[j].Date_Of_Birth : "",
          ni_number: this.fdStaff[j].ni_number ? this.fdStaff[j].ni_number : "",
          emp_start: this.fdStaff[j].Date_of_Appointment ? this.fdStaff[j].Date_of_Appointment : "",
          cont_serv_start: this.fdStaff[j].Date_of_Confirmation ? this.fdStaff[j].Date_of_Confirmation : "",
          emp_end: this.fdStaff[j].Date_of_Retirement ? this.fdStaff[j].Date_of_Retirement : "",
          apartment: this.fdStaff[j].apartment ? this.fdStaff[j].apartment : "",
          house_name: this.fdStaff[j].house_name ? this.fdStaff[j].house_name : "",
          house_number: this.fdStaff[j].house_number ? this.fdStaff[j].house_number : "",
          street: this.fdStaff[j].street ? this.fdStaff[j].street : "",
          district: this.fdStaff[j].district ? this.fdStaff[j].district : "",
          town: this.fdStaff[j].town ? this.fdStaff[j].town : "",
          county: this.fdStaff[j].county ? this.fdStaff[j].county : "",
          country: this.fdStaff[j].country ? this.fdStaff[j].country : "",
          addline1: this.fdStaff[j].addline1 ? this.fdStaff[j].addline1 : "",
          addline2: this.fdStaff[j].addline2 ? this.fdStaff[j].addline2 : "",
          addline3: this.fdStaff[j].addline3 ? this.fdStaff[j].addline3 : "",
          addline4: this.fdStaff[j].addline4 ? this.fdStaff[j].addline4 : "",
          addline5: this.fdStaff[j].addline5 ? this.fdStaff[j].addline5 : "",
          postcode: this.fdStaff[j].postcode ? this.fdStaff[j].postcode : "",
          home_phone: this.fdStaff[j].home_phone ? this.fdStaff[j].home_phone : "",
          mobile: this.fdStaff[j].Emergency_Contact_Number ? this.fdStaff[j].Emergency_Contact_Number : "",
          email: this.fdStaff[j].email ? this.fdStaff[j].email : "",
          nok_gender: this.fdStaff[j].nok_gender ? this.fdStaff[j].nok_gender : "",
          nok_title: this.fdStaff[j].nok_title ? this.fdStaff[j].nok_title : "",
          nok_forename1: this.fdStaff[j].Spouse_Name ? this.fdStaff[j].Spouse_Name : "",
          nok_forename2: this.fdStaff[j].nok_forename2 ? this.fdStaff[j].nok_forename2 : "",
          nok_forename3: this.fdStaff[j].nok_forename3 ? this.fdStaff[j].nok_forename3 : "",
          nok_surname: this.fdStaff[j].nok_surname ? this.fdStaff[j].nok_surname : "",
          nok_apartment: this.fdStaff[j].nok_apartment ? this.fdStaff[j].nok_apartment : "",
          nok_house_name: this.fdStaff[j].nok_house_name ? this.fdStaff[j].nok_house_name : "",
          nok_house_number: this.fdStaff[j].nok_house_number ? this.fdStaff[j].nok_house_number : "",
          nok_street: this.fdStaff[j].nok_street ? this.fdStaff[j].nok_street : "",
          nok_district: this.fdStaff[j].nok_district ? this.fdStaff[j].nok_district : "",
          nok_town: this.fdStaff[j].nok_town ? this.fdStaff[j].nok_town : "",
          nok_county: this.fdStaff[j].nok_county ? this.fdStaff[j].nok_county : "",
          nok_country: this.fdStaff[j].nok_country ? this.fdStaff[j].nok_country : "",
          nok_addline1: this.fdStaff[j].addline1 ? this.fdStaff[j].addline1 : "",
          nok_addline2: this.fdStaff[j].addline2 ? this.fdStaff[j].addline2 : "",
          nok_addline3: this.fdStaff[j].addline3 ? this.fdStaff[j].addline3 : "",
          nok_addline4: this.fdStaff[j].addline4 ? this.fdStaff[j].addline4 : "",
          nok_addline5: this.fdStaff[j].addline5 ? this.fdStaff[j].addline5 : "",
          nok_postcode: this.fdStaff[j].postcode ? this.fdStaff[j].postcode : "",
          nok_home_phone: this.fdStaff[j].nok_home_phone ? this.fdStaff[j].nok_home_phone : "",
          nok_work_phone: this.fdStaff[j].nok_work_phone ? this.fdStaff[j].nok_work_phone : "",
          nok_mobile: this.fdStaff[j].Emergency_Contact_Number ? this.fdStaff[j].Emergency_Contact_Number : "",
          nok_main_phone: this.fdStaff[j].nok_main_phone ? this.fdStaff[j].nok_main_phone : "",
          emp_id: this.fdStaff[j].Employee_Code ? this.fdStaff[j].Employee_Code : "",
        }
      } else if (!staff1.includes(this.fdStaff[j].Employee_forename)) {
        staff1.push(this.fdStaff[j].Employee_forename);
      }
      staff_details.push(staffDetails);
    }
    return staff_details;
  }

  parseDate(date) {
    return moment(date).format("DD/MM/YYYY");
  }

  camelCasingString(str) {
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++) {
      str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
    }
    return str.join(' ');
  }
  reset() {
    this.fInputS.nativeElement.value = '';
    this.fInputP.nativeElement.value = '';
    this.fdStu = '';
    this.fdCon = '';
    this.fdSib = '';
    this.fdCms = '';
    this.fileStudent = '';
    this.fileParent = '';
    this.fileSibling = '';
    this.fileCMSContact = '';
    this.showProgress = false;
  }
}
